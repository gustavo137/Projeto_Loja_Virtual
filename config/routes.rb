Rails.application.routes.draw do

  get 'wellcome/index'

  resources :articles
  devise_for :users
  devise_for :installs
  root 'wellcome#index'

end
